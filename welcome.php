<?php
	require_once("api.php");
	error_reporting(0);
	
	if (($_SESSION['timeout'] + 10 ) < time()) 
	{
		header("Location:sign_out.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="frontend/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"">
	<link href="frontend/css/styles.css" type="text/css" rel="stylesheet" media="screen and (min-width: 481px)">
	<link href="frontend/css/mobile_style.css" type="text/css" rel="stylesheet" media="only screen and (max-width: 480px)">
	<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="frontend/bootstrap/js/bootstrap.min.js"></script>


</head>

<body>

	<div class="header-holder">
		<?php
			print(
				"<div class=dropdown>
					<div id=myDropdown class=dropdown-content>
	    					
	 				</div>
					</div>
					<p id=userName>
					"
				.$_SESSION['name']."&nbsp".$_SESSION['lastName'].
				"</p>");
		?>
		<a href="sign_out.php" class="btn btn-lg btn-primary btn-block btn-signout" >Sign out</a>
	</div>
	<div class="container-flex" id="mainpage_form">

		<h1>Welcome</h1>
	</div>
</body>
</html>