-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 09:13 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `translator_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(15) NOT NULL,
  `lastname` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL,
  `login` datetime NOT NULL,
  `status` varchar(10) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `login`, `status`, `password`) VALUES
(2, 'Lesego', 'Mahlatsi', 'jmahatsi@gmail.com', '2020-01-15 22:10:15', 'offline', 'qw'),
(3, 'James', 'Dlomo', 'jdlomo@gmail.com', '2018-12-09 22:26:35', 'offline', 'qw'),
(4, 'thulani', 'Mbatha', 'TMbatha@@gmail.com', '2018-11-29 15:13:44', 'online', 'qw'),
(12, 'Tshepo', 'Moloi', 'TshepoM@gmail.com', '2018-12-06 16:21:14', 'online', 'qw'),
(13, 'Thabo', 'Mahlatsi', 'ThaboM@gmail.com', '2018-12-09 22:32:26', 'online', 'qw'),
(16, '0', 'Revera', 'Abhi@gmail.com', '2018-12-06 23:23:19', 'offline', 'qw'),
(17, 'Patrice', 'Motsepe', 'PM@gmail.com', '2018-12-09 23:37:34', 'online', '$2y$10$17U3WTE7'),
(18, 'Steven', 'Michaels', 'StevenM@gmail.com', '2018-12-09 23:50:11', 'online', '$2y$10$/YfgPZKn'),
(19, 'lesego', 'Mahlatsi', 'jmahatsi@gmail.com', '2020-01-14 21:41:50', 'offline', '123456789'),
(20, 'lesego', 'Mahlatsi', 'jmahatsi@gmail.com', '2020-01-15 21:54:30', 'offline', '123456789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
